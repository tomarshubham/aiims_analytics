
import pandas as pd
import re
import sqlalchemy.sql
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, DateTime, Float, BIGINT


def reformat_col_headers(df):
   col_list = list(df.columns.values)
   processed_col_list = [re.sub(r'\s+|[/@$#&()!]', '_', str(item).strip().lower()) for item in col_list]
   processed_col_list = [x.replace(r'-', '_').replace(')', '').replace('(', '').
      replace('__', '_') for x in processed_col_list]
   coldict = dict(zip(col_list, processed_col_list))
   df.rename(columns=coldict, inplace=True)
   return df


base = declarative_base()


class TableClass(base):
   __tablename__ = 'aiims_survey'
   __table_args__ = {'extend_existing': True}
   id = Column(BIGINT, primary_key=True, autoincrement=True)
   recorded_date = Column(DateTime, nullable=True)
   agree_for_part = Column(String, nullable=True)
   current_country = Column(String, nullable=True)
   nationality = Column(String, nullable=True)
   age = Column(String, nullable=True)
   gender = Column(String, nullable=True)
   current_emp_status = Column(String, nullable=True)
   highest_ed_qualification = Column(String, nullable=True)
   health_profile = Column(String, nullable=True)
   in_quarantine = Column(String, nullable=True)
   type_of_quarantine = Column(String, nullable=True)
   days_of_quarantine = Column(String, nullable=True)
   postive_for_covid = Column(String, nullable=True)
   closed_associate_lose_lives = Column(String, nullable=True)
   level_of_impact_anger = Column(String, nullable=True)
   level_of_impact_anxiety = Column(String, nullable=True)
   level_of_impact_frustation = Column(String, nullable=True)
   level_of_impact_boredom = Column(String, nullable=True)
   level_of_impact_irritatibility = Column(String, nullable=True)
   level_of_impact_stigmatization = Column(String, nullable=True)
   level_of_impact_decrease_work_efficiency = Column(String, nullable=True)
   level_of_impact_problem_in_coping_with_stress = Column(String, nullable=True)
   level_of_impact_financial_collapse = Column(String, nullable=True)
   covid_adversely_impact_on_job = Column(String, nullable=True)


def table_format(filename, date_columns):
   schemaname = "aiimsschema"
   db_string = "postgresql://{}:{}@{}/{}".format('manik', 'somepasswordmanik',
                                           'dbpostgresdev.cvlmgglpkfre.us-east-2.rds.amazonaws.com:5432',
                                           'ma_dashboard')
   engine = create_engine(db_string, paramstyle="format", connect_args={'options': '-csearch_path={}'.format(schemaname)})
   base.metadata.create_all(engine)
   print('Schema_Created...')
   df = pd.read_csv(filename)
   df = reformat_col_headers(df)
   dates = date_columns.split(',')
   reformat_dates = [re.sub(r'\s+|[/@$#&()!]', '_', str(item).strip().lower()) for item in dates]
   reformat_dates = [x.replace('-', '_').replace(')', '').replace('(', '').replace('__', '_') for x in reformat_dates]
   for i in reformat_dates:
      if i in list(df.columns):
         df[i] = pd.to_datetime(df[i], dayfirst=True)
   df.fillna(sqlalchemy.sql.null(), inplace=True)
   session1 = sessionmaker(engine)
   session = session1()
   c = 0
   for idx, row in df.iterrows():
      new_entry = TableClass(
         agree_for_part=row['agree_for_part'],
         recorded_date=row['recorded_date'],
         current_country=row['current_country'],
         nationality=row['nationality'],
         age=row['age'],
         gender=row['gender'],
         current_emp_status=row['current_emp_status'],
         highest_ed_qualification=row['highest_ed_qualification'],
         health_profile=row['health_profile'],
         in_quarantine=row['in_quarantine'],
         type_of_quarantine=row['type_of_quarantine'],
         days_of_quarantine=row['days_of_quarantine'],
         postive_for_covid=row['postive_for_covid'],
         closed_associate_lose_lives=row['closed_associate_lose_lives'],
         level_of_impact_anger=row['level_of_impact_anger'],
         level_of_impact_anxiety=row['level_of_impact_anxiety'],
         level_of_impact_frustation=row['level_of_impact_frustation'],
         level_of_impact_boredom=row['level_of_impact_boredom'],
         level_of_impact_irritatibility=row['level_of_impact_irritatibility'],
         level_of_impact_stigmatization=row['level_of_impact_stigmatization'],
         level_of_impact_decrease_work_efficiency=row['level_of_impact_decrease_work_efficiency'],
         level_of_impact_problem_in_coping_with_stress=row['level_of_impact_problem_in_coping_with_stress'],
         level_of_impact_financial_collapse=row['level_of_impact_financial_collapse'],
         covid_adversely_impact_on_job=row['covid_adversely_impact_on_job'])
      session.add(new_entry)
      session.commit()
      print('Written for Row {} of {}'.format(c, len(df)))
      c += 1
   session.close()
   print('values_added')


if __name__ == '__main__':
   date_columns = 'recorded_date'
   file = '/home/ubuntu/aiims_analytics/aiims_survey.csv'
   table_format(file, date_columns)